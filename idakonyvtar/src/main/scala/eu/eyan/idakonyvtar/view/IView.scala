package eu.eyan.idakonyvtar.view;

import java.awt.Component;

trait IView {
  def getComponent(): Component
}